package com.moyo.carzrideon.Adapters;

/**
 * Created by Nikil on 11/23/2016.
 */
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.moyo.carzrideon.Activitites.RideInfoActivity;
import com.moyo.carzrideon.Models.UserRidesModel;
import com.moyo.carzrideon.R;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class UserRidesAdapter extends RecyclerView.Adapter<UserRidesAdapter.MyViewHolder> {

    private List<UserRidesModel> moviesList;
    private Context context;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView title,source,destination,date;
        public LinearLayout parentLayout;
        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            source = (TextView) view.findViewById(R.id.dSource);
            destination = (TextView) view.findViewById(R.id.dDestination);
            date = (TextView) view.findViewById(R.id.date);
            parentLayout = (LinearLayout) view.findViewById(R.id.parentlayout);
        }
    }


    public UserRidesAdapter(List<UserRidesModel> moviesList,Context context) {
        this.moviesList = moviesList;
        this.context = context;

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread paramThread, Throwable paramThrowable) {
                Log.e("Alert", "Lets See if it Works !!!");
                paramThrowable.printStackTrace();
            }
        });

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_userrides, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final UserRidesModel movie = moviesList.get(position);

       if(movie.getStatus()!=null &&movie.getStatus().equalsIgnoreCase("0"))
       {
           String first ="You requested a ride with "+movie.getName()+" and the status is" ;
           String next = "<font color='#F44336'> PENDING</font>";
           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
               holder.title.setText(Html.fromHtml(first + next,0));
           }
           else
           {
               holder.title.setText(Html.fromHtml(first + next));
           }
       }
        else if(movie.getStatus()!=null && movie.getStatus().equalsIgnoreCase("1"))
       {
           String first ="You requested a ride with "+movie.getName()+" and the status is" ;
           String next = "<font color='#F44336'> ACCEPTED</font>";
           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
               holder.title.setText(Html.fromHtml(first + next,0));
           }
           else
           {
               holder.title.setText(Html.fromHtml(first + next));
           }
       }
       else if(movie.getStatus()!=null &&movie.getStatus().equalsIgnoreCase("2"))
       {
           String first ="You requested a ride with "+movie.getName()+" and the status is" ;
           String next = "<font color='#F44336'> REJECTED</font>";
           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
               holder.title.setText(Html.fromHtml(first + next,0));
           }
           else
           {
               holder.title.setText(Html.fromHtml(first + next));
           }
       }

        holder.source.setText(movie.getSource()+"");
        holder.destination.setText(movie.getDestination()+"");
        holder.date.setText("Request time: "+converDateTime(movie.getRide_date()));
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                context.startActivity(new Intent(context, RideInfoActivity.class).putExtra("acceptedobject",movie));
            }
        });

    }

    public String converDateTime(String dateTime) {
        try {


            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = f.parse(dateTime);
            String day = (String) android.text.format.DateFormat.format("dd", date);
            String stringMonth = (String) android.text.format.DateFormat.format("MMM", date);
            String hours = (String) android.text.format.DateFormat.format("HH", date);
            String minutes = (String) android.text.format.DateFormat.format("mm", date);
            Log.d("time", date + "");
            return day + " " + stringMonth + ", " + hours + ":" + minutes;
        } catch (Exception e) {
            return "";
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }

    public void updateRefernceStatus()
    {

    }

}
